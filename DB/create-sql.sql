CREATE TABLE `test`.`tag` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `test`.`article` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(100) NULL,
  `date` TIMESTAMP NULL,
  `body` VARCHAR(3000) NULL,
  PRIMARY KEY (`id`));

CREATE TABLE `test`.`article_tag` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `article_id` INT NOT NULL,
  `tag_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `tag_id_idx` (`tag_id` ASC),
  INDEX `article_id_idx` (`article_id` ASC),
  CONSTRAINT `article_id`
    FOREIGN KEY (`article_id`)
    REFERENCES `test`.`article` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `tag_id`
    FOREIGN KEY (`tag_id`)
    REFERENCES `test`.`tag` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION);

