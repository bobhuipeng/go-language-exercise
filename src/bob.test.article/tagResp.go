package main


type TegResp struct {
	Tag string   	`json:"tag"`
	Count  int   `json:"count"`
	Articles  []int64   `json:"articles"`
	RelatedTags  []string `json:"related_tags"`
}


type Tag struct {
	Id   int64  `json:"id"`
	Name string `json:"name"`
}