package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
)

func Index(rw http.ResponseWriter, req *http.Request) {
	fmt.Fprint(rw, "Welcome to Fairfax Media!\n")
}

/*
 */
func AddArticle(rw http.ResponseWriter, req *http.Request) {
	var article Article
	//limit the body size to avoid receive large file attack
	//TODO: limit size should read from config
	body, err := ioutil.ReadAll(io.LimitReader(req.Body, 1048576))
	if err != nil {
		panic(err)
	}

	if err := req.Body.Close(); err != nil {
		panic(err)
	}

	//TODO: add json validation
	if err := json.Unmarshal(body, &article); err != nil {
		setResponse(http.StatusUnprocessableEntity, err, rw)
		return
	}

	art := AddArticleToStorage(article)

	if art.Id > 0 {
		setResponse(http.StatusOK, art, rw)
		return
	}

	// If the Article is not success saved, return status 500
	setResponse(http.StatusInternalServerError, RespError{Code: http.StatusInternalServerError, Text: "Save Article failed!"}, rw)
}

func ShowArticle(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	var articleId int
	var err error

	//validate the query ID is a number
	if articleId, err = strconv.Atoi(vars["articleId"]); err != nil {
		setResponse(http.StatusBadRequest, RespError{Code: http.StatusBadRequest, Text: "The query ID ("+vars["articleId"]+") is not a number."}, rw)
		return
	}

	article := FindArticle(int64(articleId))

	//finded article
	if article.Id > 0 {
		setResponse(http.StatusOK, article, rw)
		return
	}

	// If we didn't find it, 404
	setResponse(http.StatusNotFound, RespError{Code: http.StatusNotFound, Text: "Not Found"}, rw)
}

/**
*API url: articles/{tagName}/{date}
*date : formate "yyyymmdd"
*
 */
func SearchTag(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)

	if date, err := time.Parse("20060102", vars["date"]); err != nil {
		setResponse(http.StatusBadRequest, RespError{Code: http.StatusBadRequest, Text: "Query Date ("+vars["date"]+") format is not 'yyyymmdd'."}, rw)

	} else { //the date parameter formate is correct
		tags := FindTags(vars["tagName"], date)
		setResponse(http.StatusOK, tags, rw)
	}

}

func setResponse(statusId int, resBody interface{}, rw http.ResponseWriter) {
	rw.Header().Set("Content-Type", "application/json; charset=UTF-8")
	rw.WriteHeader(statusId)
	if err := json.NewEncoder(rw).Encode(resBody); err != nil {
		panic(err)
	}
}
