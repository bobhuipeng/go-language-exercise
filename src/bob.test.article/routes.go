package main

import "net/http"

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"AddArticle",
		"POST",
		"/articles",
		AddArticle,
	},	
	Route{
		"FindArticle",
		"GET",
		"/articles/{articleId}",
		ShowArticle,
	},
	Route{
		"FindTag",
		"GET",
		"/articles/{tagName}/{date}",
		SearchTag,
	},
}
