package main

import (
	"fmt"
	"strings"
)

/**
*
 */
func Filter(vs []string, f func(string) bool) []string {
	vsf := make([]string, 0)
	for _, v := range vs {
		if f(v) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

/**
*
 */
func UniqueSlice(s []string) []string {
	m := make(map[string]bool)
	for _, item := range s {
		if !m[item] {
			m[item] = true
		}
	}

	var result []string
	for item, _ := range m {
		result = append(result, item)
	}
	return result
}

func arrayToString(a []int64, delim string) string {
	return strings.Trim(strings.Join(strings.Split(fmt.Sprint(a), " "), delim), "[]")
}

//func Unit64ToInt64(un Uint64) int64 {
//	size := binary.BigEndian.Uint64(b[4:])
//	n, _ := rdr.Discard(int64(size))
//	return n
//}
