package main

type RespError struct {
	Code int    `json:"code"`
	Text string `json:"text"`
}
