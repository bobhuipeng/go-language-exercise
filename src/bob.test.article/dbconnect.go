package main

import (
	"fmt"
	"time"

	"github.com/siddontang/go-mysql/client"
)

var connG *client.Conn

const MAX_ARTICLES_FOR_TAGS int = 10

func init() {
	fmt.Println("Create Connection to My SQL!")
	//TODO: add connection in config
	conn, _ := client.Connect("127.0.0.1:3306", "root", "", "test")
	connG = conn
	conn.Ping()
	fmt.Println("My SQL Connection has been built!")
}

func executeSql(sql string) {
	//TODO: add exception control
	connG.Execute(sql)
}

func insertArticle(article Article) Article {
	var insertSql = fmt.Sprintf("insert into article ( title,date,body) values ( '%s',TIMESTAMP('%s') , '%s')", article.Title, article.Date, article.Body)
	//TODO: add exception control
	r, _ := connG.Execute(insertSql)
	article.Id = int64(r.InsertId)

	//TODO: put insert article, tag and article_tag in same transaction
	if article.Id > 0 {
		insertArticleTag(article)
	}

	return article
}

func searchTag(tagName string) Tag {
	var searchSql = fmt.Sprintf("select * from tag where name = '%s'", tagName)
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)
	//	article.Id = r.InsertId r.RowDatas


	var tag Tag
	if len(r.RowDatas) > 0 {
		//TODO: add exception control
		id, _ := r.GetIntByName(0, "id")
		tag.Id = id
		//TODO: add exception control
		name, _ := r.GetStringByName(0, "name")
		tag.Name = name
	}

	return tag
}

func searchArticle(articleId int64) Article {
	var searchSql = fmt.Sprintf("select * from article where id = %d", articleId)
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)
	//	article.Id = r.InsertId r.RowDatas


	var article Article

	if len(r.RowDatas) > 0 {
		//TODO: add exception control
		id, _ := r.GetIntByName(0, "id")
		article.Id = id

		title, _ := r.GetStringByName(0, "title")
		article.Title = title

		body, _ := r.GetStringByName(0, "body")
		article.Body = body

		dateStr, _ := r.GetStringByName(0, "date")
		article.Date = dateStr[0:10]

		article.Tags = searchTagNamesByArticleId(id)
	} else {
		article.Tags = []string{}
	}

	return article
}

func insertTag(tagName string) Tag {
	var tag Tag
	var insertSql = fmt.Sprintf("insert into tag ( name) values ( '%s')", tagName)
	//TODO: add exception control
	r, _ := connG.Execute(insertSql)

	tag.Id = int64(r.InsertId)
	tag.Name = tagName
	return tag
}

func searchAndInsertTag(tagName string) Tag {

	tag := searchTag(tagName)

	if tag.Id < 1 {
		tag = insertTag(tagName)
	}
	return tag
}

func insertArticleTag(article Article) {

	for _, tagName := range article.Tags {
		tag := searchAndInsertTag(tagName)
		var insertSql = fmt.Sprintf("insert into article_tag ( article_id,tag_id) values ( %d,%d)", article.Id, tag.Id)

		//TODO: add exception control
		 connG.Execute(insertSql)


	}

	//	return article
}

func searchTagsByArticleIds(articleIds []int64) []Tag {

	var searchSql = fmt.Sprintf("select DISTINCT t.id, t.name from tag t, article_tag art where art.article_id in (%s)  and art.tag_id = t.id", arrayToString(articleIds, ","))
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)
	//	article.Id = r.InsertId r.RowDatas


	var tags []Tag

	for n := 0; n < len(r.RowDatas); n++ {
		var tag Tag
		//TODO: add exception control
		id, _ := r.GetIntByName(n, "id")
		tag.Id = id

		//TODO: add exception control
		name, _ := r.GetStringByName(n, "name")
		tag.Name = name

		tags = append(tags, tag)
	}

	return tags

}

func searchTagNamesByArticleId(articleId int64) []string {
	var searchSql = fmt.Sprintf("select DISTINCT t.id, t.name from tag t, article_tag art where art.article_id = %d  and art.tag_id = t.id", articleId)
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)
	//	article.Id = r.InsertId r.RowDatas


	var tagNames []string

	for n := 0; n < len(r.RowDatas); n++ {
		name, _ := r.GetStringByName(n, "name")

		tagNames = append(tagNames, name)
	}

	return tagNames
}

func searchTagNamesByArticleIds(articleIds []int64) []string {

	var searchSql = fmt.Sprintf("select DISTINCT t.id, t.name from tag t, article_tag art where art.article_id in (%s)  and art.tag_id = t.id", arrayToString(articleIds, ","))
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)
	//	article.Id = r.InsertId r.RowDatas


	var tagNames []string

	for n := 0; n < len(r.RowDatas); n++ {
		name, _ := r.GetStringByName(n, "name")

		tagNames = append(tagNames, name)
	}

	return tagNames

}

func countTagsByArticleIds(articleIds []int64) int {

	var searchSql = fmt.Sprintf("select DISTINCT t.id, t.name from tag t, article_tag art where art.article_id in (%s)  and art.tag_id = t.id", arrayToString(articleIds, ","))
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)
	//	article.Id = r.InsertId r.RowDatas


	return len(r.RowDatas)

}

func searchArticleIds(tagName string, date time.Time) []int64 {
	var searchSql = fmt.Sprintf("select ar.id from article ar, tag t, article_tag art where art.article_id = ar.id and art.tag_id = t.id and ar.date = TIMESTAMP('%s') and  t.name = '%s'  order by ar.id desc", date.Format("2006-01-02"), tagName)
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)

	var allArticleIds []int64

	for n := 0; n < len(r.RowDatas); n++ {
		article_id, _ := r.GetIntByName(n, "id")
		allArticleIds = append(allArticleIds, article_id)
	}

	return allArticleIds
}

func searchTagsSummary(tagName string, date time.Time) TegResp {
	var searchSql = fmt.Sprintf("select * from article ar, tag t, article_tag art where art.article_id = ar.id and art.tag_id = t.id and ar.date = TIMESTAMP('%s') and  t.name = '%s'  order by ar.id desc", date.Format("2006-01-02"), tagName)
	//TODO: add exception control
	r, _ := connG.Execute(searchSql)

	var allArticleIds, displayArticlesIds []int64

	for n := 0; n < len(r.RowDatas); n++ {
		article_id, _ := r.GetIntByName(n, "article_id")
		if n < MAX_ARTICLES_FOR_TAGS {
			displayArticlesIds = append(displayArticlesIds, article_id)
		}
		allArticleIds = append(allArticleIds, article_id)
	}
	//	article.Id = r.InsertId r.RowDatas


	var tags TegResp
	tags.Tag = tagName
	if len(allArticleIds) > 0 {
		tagNames := searchTagNamesByArticleIds(allArticleIds)

		tags.Articles = displayArticlesIds

		tags.Count = len(tagNames)

		tags.RelatedTags = Filter(tagNames, func(v string) bool {
			return v != tagName
		})
	}

	return tags
}
