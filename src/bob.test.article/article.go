package main

//import "time"

type Article struct {
	Id    int64    `json:"id"`
	Title string   `json:"title"`
	Date  string   `json:"date"`
	Body  string   `json:"body"`
	Tags  []string `json:"tags"`
}

type Artiles []Article
