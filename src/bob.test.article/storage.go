package main

import (
	"time"
	//	"fmt"
)

//TODO: use database instead of the temp array
var articles Artiles

var currentId int

const MAX_ARTICLES_NO_FOR_TAGS int = 10

/**
* init some test data
 */
func init() {
	AddArticleToStorage(Article{Title: "Doctor slain by cycling assassin", Date: "2018-05-06", Body: "This is the article body - simple test", Tags: []string{"health", "fitness", "science", "weather"}})
	AddArticleToStorage(Article{Title: "The American city that will surprise you", Date: "2018-05-06", Body: "This is the article body - simple test", Tags: []string{"health", "fitness", "science", "travel"}})
	AddArticleToStorage(Article{Title: "The Aussie city we have ruined", Date: "2018-07-06", Body: "This is the article body - simple test", Tags: []string{"health", "fitness", "science", "sport"}})
	AddArticleToStorage(Article{Title: "The people quitting six-figure jobs", Date: "2018-07-06", Body: "This is the article body - simple test", Tags: []string{"world", "lifestyle", "finance", "sport"}})
	AddArticleToStorage(Article{Title: "Stars exile after alleged affair", Date: "2018-07-06", Body: "This is the article body - simple test", Tags: []string{"national", "education", "job", "sport"}})
}

/**
* add article to storage.
 */
func AddArticleToStorage(article Article) Article {
	return insertArticle(article)
}

/**
* find article by ID.
* If not find article, return an empty Article object
 */
func FindArticle(id int64) Article {

	// return empty article if not found
	return searchArticle(id)
}

/**
* Find tags by tagname and date
 */
func FindTags(tagName string, date time.Time) TegResp {
	var tagResp TegResp
	tagResp.Tag = tagName

	allArticleIds := searchArticleIds(tagName, date)

	if len(allArticleIds) > 0 {
		tagNames := searchTagNamesByArticleIds(allArticleIds)

		if len(allArticleIds) > MAX_ARTICLES_FOR_TAGS {
			tagResp.Articles = allArticleIds[0:MAX_ARTICLES_NO_FOR_TAGS]
		} else {
			tagResp.Articles = allArticleIds
		}

		tagResp.Count = len(tagNames)

		tagResp.RelatedTags = Filter(tagNames, func(v string) bool {
			return v != tagName
		})
	}

	return tagResp
}
